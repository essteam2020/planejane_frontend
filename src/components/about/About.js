import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPaperPlane} from "@fortawesome/free-solid-svg-icons";
import './About.css';

class About extends React.Component {

    render() {
        return (
            <div className="wrapperAbout">
                <FontAwesomeIcon icon={faPaperPlane} size="10x" className="fa-icon-nice" />
                <h3 className="titleAbout">Thank you for choosing PlaneJane</h3>
                <p className="description">Hi, the PlaneJane team greets you, hope you're having a fantastic experience.</p>
                <p className="description">If you want to know more about us and the developing of this system please click <a href="https://essteam2020.bitbucket.io/">here</a>.</p>
                <p className="description">&copy; Work done by the PlaneJane team. All rights reserved.</p>
                <p className="description">See you soon!</p>
            </div>
        );
    }
}

export default About;
