import React from "react";
import './Alerts.css';

class Alerts extends React.Component {

    render() {
        return (
            <div className="alertsContainer">
                <table className="alertsTable">
                    <thead className="tableHead">
                    <tr>
                        <th>INFO</th>
                        <th>PLANES</th>
                        <th>DATE</th>
                    </tr>
                    </thead>

                    <tbody className="tableBody">
                        {
                            this.props.notifications.map((notification, index) => (
                            <tr key={index}>
                                <td className="colInfo">
                                    <strong>{notification.icaos.length}</strong> landed on <strong>{notification.country}</strong>!
                                </td>
                                <td className="colInfo">
                                    <ul className="planesList">
                                        {notification.icaos.map((plane, key) => (<li className="planeElement" key={key}>{plane}</li>))}
                                    </ul>

                                </td>
                                <td className="colDate">{notification.date}</td>


                            </tr>
                        ))}
                    </tbody>

                </table>
            </div>
        );
    }
}

export default Alerts;