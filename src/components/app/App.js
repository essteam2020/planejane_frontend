import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Navbar from "../navbar/Navbar";
import Home from "../home/Home";
import Statistics from "../statistics/Statistics";
import Alerts from "../alerts/Alerts";
import SockJsClient from "react-stomp";
import About from "../about/About";



class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            clock: false,
            planes: [],
            notifications: [],
        };
        this.socketConnect = this.socketConnect.bind(this);
        this.socketConnectFailure = this.socketConnectFailure.bind(this);
        this.handleNotification = this.handleNotification.bind(this);
    }

    handleNotification(data) {
        let entries = Object.entries(data);

        let niceData = [];
        let planesLanded = 0;
        let countries = 0;

        for(const [country, icaos] of entries) {
            planesLanded += icaos.length;
            countries += 1;
            niceData.push({country:country, icaos:icaos, date: new Date().toString()})
        }
        this.setState({notifications: [...niceData, ...this.state.notifications]});
        alert("Hey, " + planesLanded + " planes landed on " + countries + " different countries!");
    }

    socketConnect() {
        console.log("Successful socket connection http://192.168.160.20:55080/landings_socket. Alerts will be arriving soon...");
    }

    socketConnectFailure() {
        console.log("Failed socket connection to http://192.168.160.20:55080/landings_socket. No alerts will appear...");
    }

    fetchPlanes() {
        fetch('http://192.168.160.20:55080/flights')
            .then(res => res.json())
            .then((data) => {
                this.setState({
                    isLoaded: true,
                    planes: data
                })
            })
            .catch((e) => {
                console.log(e);
                this.setState({
                    isLoaded: true,
                    planes: [],
                    error: true
                })
            })
    }

    componentDidMount() {
        this.fetchPlanes();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(!this.state.clock) {
            this.setState({clock:true});
            setTimeout(() => {
                this.fetchPlanes();
                this.setState({clock:false});
            }, 600 * 1000); //10 mins
        }
    }

    render() {
        return (
            <Router>
                <Navbar />
                <main>
                    <SockJsClient url='http://192.168.160.20:55080/landings_socket' topics={['/landings']}
                                  onConnect={this.socketConnect}
                                  onConnectFailuer={this.socketConnectFailure}
                                  onMessage={this.handleNotification}/>
                    <Switch>
                        <Route path="/" exact={true}  component={() => <Home planes={this.state.planes} isLoaded={this.state.isLoaded} error={this.state.error}/>} />
                        <Route path="/statistics" component={() => <Statistics planes={this.state.planes} isLoaded={this.state.isLoaded} error={this.state.error} />} />
                        <Route path="/arrivals" component={() => <Alerts notifications={this.state.notifications}/>} />
                        <Route path="/info" component={() => <About />} />
                    </Switch>
                </main>
            </Router>
        );
    }


}

export default App;
