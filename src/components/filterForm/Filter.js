import React from 'react';
import './Filter.css';

const hemisphereCoordinates = {
    all: {latitude:[-90,90],longitude:[-180,180]},
    north: {latitude:[0,90],longitude:[-180,180]},
    south: {latitude:[-90,0],longitude:[-180,180]},
    east: {latitude:[-90,90],longitude:[0,180]},
    west: {latitude:[-90,90],longitude:[-180,0]}
};

class Filter extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            planeLimit: '9999',
            planeArea: 'all',
        };

        this.handlePlaneLimitChange = this.handlePlaneLimitChange.bind(this);
        this.handlePlaneAreaChange = this.handlePlaneAreaChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handlePlaneLimitChange(event) {
        this.setState({planeLimit: event.target.value});
    }

    handlePlaneAreaChange(event) {
        this.setState({planeArea: event.target.value});
    }

    handleSubmit(event) {
        this.props.handleSubmit(parseInt(this.state.planeLimit), hemisphereCoordinates[this.state.planeArea].latitude, hemisphereCoordinates[this.state.planeArea].longitude);
        event.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>Max number of planes:
                    <select value={this.state.value} onChange={this.handlePlaneLimitChange}>
                        <option value="9999">9999</option>
                        <option value="100">100</option>
                        <option value="500">500</option>
                        <option value="1000">1000</option>
                        <option value="2000">2000</option>
                    </select>
                </label>

                <label>Planes Currently in:
                    <select value={this.state.value} onChange={this.handlePlaneAreaChange}>
                        <option value="all">All</option>
                        <option value="north">North Hemisphere</option>
                        <option value="south">South Hemisphere</option>
                        <option value="west">West Hemisphere</option>
                        <option value="east">East Hemisphere</option>
                    </select>
                </label>

                <input type="submit" value="Filter!" />
            </form>
        );
    }
}

export default Filter;