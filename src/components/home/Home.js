import React from 'react';
import './Home.css';
import { Map, Marker, TileLayer, Polyline } from "react-leaflet";
import L from 'leaflet';
import Modal from "react-modal";
import Loading from "../loading/Loading";
import PlaneInfoModal from "../planeInfoModal/PlaneInfoModal";
import Filter from "../filterForm/Filter";


const planeIcon = new L.Icon({
    iconUrl: require('../../images/plane-solid.svg'),
    iconRetinaUrl: require('../../images/plane-solid.svg'),
    iconSize: [16, 16],
});

const center = [0, 0];
const minZoom = 3;

class Home extends React.Component {

    constructor(props) {
        super(props);

        //MODAL
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        //FILTER
        this.handleFilter = this.handleFilter.bind(this);

        this.state = {
            lat: 0,
            lng: 0,
            zoom: 3,
            showModal: false,
            plane: null,
            planes: props.planes,
            showline: false,
            line: [[0, 0], [0, 0]]
        };
        Modal.setAppElement("#root");
    }

    openModal(plane) {  // switch the value of the showModal state
        this.setState({
            showModal: true,
            plane: plane,
        })
    }

    closeModal() {
        this.setState({
            showModal: false,
            plane: null,
        })
    }

    getComponent() {
        if (this.state.showModal) {  // show the modal if state showModal is true
            return <PlaneInfoModal plane={this.state.plane} onRequestClose={this.closeModal} />;
        } else {
            return null;
        }
    }

    getPosition(plane) {
        return [plane.latitude, plane.longitude];
    }

    /* FILTER */
    filterByLatLong(planesList, latitude, longitude) {
        const [lowerLatitude, upperLatitude] = latitude;
        const [lowerLongitude, upperLongitude] = longitude;

        if (latitude.length > 0) {
            planesList = planesList.filter(plane => plane.latitude >= lowerLatitude && plane.latitude <= upperLatitude);

        }

        if (longitude.length > 0) {
            planesList = planesList.filter(plane => plane.longitude >= lowerLongitude && plane.longitude <= upperLongitude);
        }

        return planesList;
    }

    handleFilter(planeLimit, latitude, longitude) {
        let newPlanes = this.props.planes;

        newPlanes = this.filterByLatLong(newPlanes.slice(0, planeLimit), latitude, longitude);

        this.setState({
            planes: newPlanes
        })
    }
    /* FILTER END */

    /* Adiciona a linha da rota quando clica num Marker */
    showLine = (icao24) => {

        // Fetch previous coordinates
        fetch('http://192.168.160.20:55080/realtime/track?icao24=' + icao24)
            .then(res => res.json())
            .then((coordinatesDocs) => {
                // Transform to array of arrays
                const lineDots = coordinatesDocs.map(coordinate => {
                    return [coordinate.latitude, coordinate.longitude]
                })
                this.setState({
                    showline: true,
                    line: lineDots
                })
            })
            .catch((e) => {
                console.log(e);
            })
    };

    /* Remove a linha da rota quando clica no mapa (fora de um Marker) */
    removeLine = () => {
        this.setState({ showline: false })
    };

    render() {
        if (!this.props.isLoaded) {
            return <div className="loading"><Loading width="40px" height="100px" /><h1>Fetching planes data . . .</h1></div>
        }

        return (

            <div className="pageContainer">
                <Map ref={(ref) => { this.map = ref; }} center={center} zoom={this.state.zoom} preferCanvas={true} minZoom={minZoom} animate={true} onClick={() => { this.removeLine() }}>
                    <TileLayer
                        attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    {
                        this.state.planes.map((plane, index) => (
                            <div key={index}>
                                <Marker icon={planeIcon} riseOnHover={true} position={this.getPosition(plane)} onClick={() => {
                                    this.showLine(plane.icao24);
                                    this.openModal(plane);
                                }}>
                                </Marker>
                            </div>
                        ))}
                    {this.state.showline ? <Polyline color="green" positions={this.state.line} /> : null}

                </Map>
                {this.getComponent()}
                <Filter handleSubmit={this.handleFilter} className="filter" />
            </div>
        );
    }
}

export default Home;