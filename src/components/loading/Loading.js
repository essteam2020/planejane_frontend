import React from 'react';
import ScaleLoader from "react-spinners/ScaleLoader";

class Loading extends React.Component {

    render() {
        return (
                <ScaleLoader
                    className={"loader"}
                    height={this.props.height}
                    width={this.props.width}
                    radius={5}
                    margin={5}
                    color={"#28b76b"}
                    loading={true}
                />
        );
    }


}

export default Loading;