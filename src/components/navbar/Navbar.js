import React from 'react';
import './Navbar.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {Link} from "react-router-dom";
import {faInfoCircle, faPlane, faPlaneArrival, faChartBar, faGlobeAfrica} from "@fortawesome/free-solid-svg-icons";
import Home from "../home/Home";
import Statistics from "../statistics/Statistics";
import Alerts from "../alerts/Alerts";
import About from "../about/About";

const routes = [
    {
        path: "/",
        exact: true,
        icon: faGlobeAfrica,
        name: "Map",
        component: Home
    },
    {
        path: "/statistics",
        icon: faChartBar,
        name: "Statistics",
        component: Statistics
    },
    {
        path: "/arrivals",
        icon: faPlaneArrival,
        name: "Arrivals",
        component: Alerts
    },
    {
        path: "/info",
        icon: faInfoCircle,
        name: "About",
        component: About
    }
];

const logo = faPlane;

class App extends React.Component {

    constructor(props) {
        super(props);

        this.state = {};

    }

    render() {
        return (
            <nav className="navbar">
                <ul className="navbar-nav">
                    <li className="logo">
                        <Link to="/" className="nav-link">
                            <span className="link-text">PlaneJane</span>
                            <FontAwesomeIcon icon={logo} size="2x" className="fa-icon"/>
                        </Link>
                    </li>

                    {
                        routes.map((route, index) => (
                        <li key={index} className="nav-item">
                            <Link to={route.path} className="nav-link">
                                <FontAwesomeIcon icon={route.icon} size="lg" className="fa-icon" />
                                <span className="link-text">{route.name}</span>
                            </Link>
                        </li>
                    ))}

                </ul>
            </nav>
        );
    }
}

export default App;
