import React from 'react';
import './NotFound.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faPaperPlane} from "@fortawesome/free-solid-svg-icons";

class NotFound extends React.Component {

    render() {
        return (
            <div className="main">
                <FontAwesomeIcon icon={faPaperPlane} size="10x" />
                <h1 className="title">Under Development</h1>
                <p className="description">Currently under development. Try again later!</p>
            </div>
        );
    }
}

export default NotFound;
