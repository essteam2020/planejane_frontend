import React from 'react';
import Modal from "react-modal";
import {
    faArrowsAltH, faArrowsAltV, faBarcode,
    faBroadcastTower, faCampground, faFlag,
    faHeadset, faHourglassHalf, faIdCard,
    faRuler, faRulerCombined, faRulerVertical,
    faSatelliteDish, faTachometerAlt, faThumbtack
} from "@fortawesome/free-solid-svg-icons";
import './PlaneInfoModal.css';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

class PlaneInfoModal extends React.Component {

    constructor(props) {
        super(props);
        Modal.setAppElement("#root");
    }

    render() {
        const plane = this.props.plane;

        return (
            <Modal
                contentLabel="Info"
                isOpen={true}
                onRequestClose={this.props.onRequestClose}
                shouldCloseOnOverlayClick={false}
                closeTimeoutMS={0}
                className="modal-content">

                <h1 className="modal-title">Plane Info</h1>
                <ul className="modal-list">
                    <li className="modal-item"><b><FontAwesomeIcon icon={faIdCard} size="lg" className="fa-icon-modal" />Icao24:</b> {plane.icao24}</li>
                    <li className="modal-item"><b><FontAwesomeIcon icon={faHeadset} size="lg" className="fa-icon-modal" />Callsign:</b> {plane.callsign ? plane.callsign : "Unkwown"}</li>
                    <li className="modal-item"><b><FontAwesomeIcon icon={faFlag} size="lg" className="fa-icon-modal" />Origin country:</b> {plane.origin_country}</li>
                    <li className="modal-item"><b><FontAwesomeIcon icon={faHourglassHalf} size="lg" className="fa-icon-modal" />Time position:</b> {plane.time_position}</li>
                    <li className="modal-item"><b><FontAwesomeIcon icon={faSatelliteDish} size="lg" className="fa-icon-modal" />Last contact:</b> {plane.last_contact}</li>
                    <li className="modal-item"><b><FontAwesomeIcon icon={faArrowsAltH} size="lg" className="fa-icon-modal" />Latitude:</b> {plane.latitude}</li>
                    <li className="modal-item"><b><FontAwesomeIcon icon={faArrowsAltV} size="lg" className="fa-icon-modal" />Longitude:</b> {plane.longitude}</li>
                    <li className="modal-item"><b><FontAwesomeIcon icon={faRulerCombined} size="lg" className="fa-icon-modal" />Baro altitude:</b> {plane.baro_altitude}</li>
                    <li className="modal-item"><b><FontAwesomeIcon icon={faCampground} size="lg" className="fa-icon-modal" />On ground:</b> {plane.on_ground ? "Yes" : "No"}</li>
                    <li className="modal-item"><b><FontAwesomeIcon icon={faTachometerAlt} size="lg" className="fa-icon-modal" />Velocity:</b> {plane.velocity}</li>
                    <li className="modal-item"><b><FontAwesomeIcon icon={faThumbtack} size="lg" className="fa-icon-modal" />True track:</b> {plane.true_track}</li>
                    <li className="modal-item"><b><FontAwesomeIcon icon={faRuler} size="lg" className="fa-icon-modal" />Vertical rate:</b> {plane.vertical_rate}</li>
                    <li className="modal-item"><b><FontAwesomeIcon icon={faRulerVertical} size="lg" className="fa-icon-modal" />Geo altitude:</b> {plane.geo_altitude}</li>
                    <li className="modal-item"><b><FontAwesomeIcon icon={faBarcode} size="lg" className="fa-icon-modal" />Squawk:</b> {plane.squawk  !== "null" ? plane.squawk : "Unkwown"}</li>
                    <li className="modal-item"><b><FontAwesomeIcon icon={faBroadcastTower} size="lg" className="fa-icon-modal" />Position source:</b> {plane.position_source}</li>
                    <button className="modal-close" onClick={this.props.onRequestClose}>Close</button>
                </ul>

            </Modal>
        );
    }


}

export default PlaneInfoModal;