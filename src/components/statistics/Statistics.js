import React from "react";
import './Statistics.css';
import {ResponsivePie} from "@nivo/pie";
import { ResponsiveChoropleth } from '@nivo/geo'
import Filter from "../filterForm/Filter";
import Loading from "../loading/Loading";
import countries from "../../data/countries";
import features from "../../data/features_countries";
import {ResponsiveBar} from "@nivo/bar";

class Statistics extends React.Component {

    controller = new AbortController();

    constructor(props) {
        super(props);

        this.emptyArray = [];
        this.state = {
            onGroundStats:[],
            originCountryStats:[],
            velocityStats:[],
            commonRoutes: [],
            planes: props.planes,
        };
        this.handleFilter = this.handleFilter.bind(this);
    }

    componentDidMount() {
        if(this.props.isLoaded) {
            this.createOnGroundStats(this.state.planes);
            this.createOriginCountryStats(this.state.planes);
            this.createVelocityStats(this.state.planes);
            this.fetchCommonRoutes();
        }
    }

    componentWillUnmount() {
        this.controller.abort();
    }

    fetchCommonRoutes() {
        fetch('http://192.168.160.20:55080/statistics/common-routes?limit=1000', this.controller)
            .then(res => res.json())
            .then((data) => {
                this.setState({
                    commonRoutes: data,
                })
            })
            .catch((e) => {
                if(e.name === "AbortError") {
                    return;
                }
                this.setState({
                    commonRoutes: []
                })
            })
    }

    createOnGroundStats(planesList) {
        const ground = {
            "id": "ground",
            "label": "On ground",
            "value": planesList.filter(plane => plane.on_ground).length,
        };
        const air = {
            "id": "air",
            "label": "On air",
            "value": planesList.filter(plane => !plane.on_ground).length,
        };

        this.setState({
            onGroundStats : [...this.emptyArray, air, ground]
        });
    }

    createOriginCountryStats(planesList) {
        let countryStats = {};

        countries.map(country => countryStats[country.name] = {count: 0, code: country.code});

        for(let i = 0; i < planesList.length; i++) {
            const key = planesList[i].origin_country;
            if(!countryStats[key]) {
                continue;
            }
            const count = countryStats[key].count + 1;
            const code = countryStats[key].code;
            countryStats[key] = {count: count, code: code};

        }

        let data = [];

        for (let country in countryStats) {
            data.push({"id": countryStats[country].code, "value":countryStats[country].count});
        }

        this.setState({
            originCountryStats : [...this.emptyArray, ...data]
        });
    }

    createVelocityStats(planesList) {
        const step = 25;
        const maxIndex = 10;

        let velocityStats = [
            {velocity:"0", count:0},
        ];

        for (let k = 1; k < maxIndex; k++) {
            const previous = (k-1)*step+1;
            const next = k*step;
            velocityStats.push({velocity:previous+"-"+next, count:0})
        }

        velocityStats.push({velocity:maxIndex*step+1+"+", count:0});

        for(let i = 0; i < planesList.length; i++) {
            let indexVelocity = Math.floor(planesList[i].velocity / step);
            if(indexVelocity > maxIndex) {indexVelocity = maxIndex}
            velocityStats[indexVelocity].count += 1;
        }

        this.setState({
            velocityStats : [...this.emptyArray, ...velocityStats]
        });
    }

    /* FILTER */
    filterByLatLong(planesList, latitude, longitude) {
        const [lowerLatitude, upperLatitude] = latitude;
        const [lowerLongitude, upperLongitude] = longitude;

        if(latitude.length > 0) {
            planesList = planesList.filter(plane => plane.latitude >= lowerLatitude && plane.latitude <= upperLatitude);

        }

        if(longitude.length > 0) {
            planesList = planesList.filter(plane => plane.longitude >= lowerLongitude && plane.longitude <= upperLongitude);
        }

        return planesList;
    }

    handleFilter(planeLimit, latitude, longitude) {
        let newPlanes = this.props.planes;

        /* Plane limit & Latitude Longitude*/
        newPlanes = this.filterByLatLong(newPlanes.slice(0,planeLimit), latitude, longitude);

        this.createOnGroundStats(newPlanes);

        this.createOriginCountryStats(newPlanes);

        this.createVelocityStats(newPlanes);

        this.setState({
            planes : newPlanes
        })
    }
    /* FILTER END */

    render() {
        if(!this.props.isLoaded) {
            return <div className="loading"><Loading width="40px" height="100px" /><h1>Fetching planes data . . .</h1></div>
        }

        const onGroundStatsPie = <ResponsivePie
            data={this.state.onGroundStats}
            margin={{ top: 40, right: 200, bottom: 40, left: 20 }}
            enableRadialLabels={false}
            innerRadius={0.5}
            padAngle={0.7}
            cornerRadius={5}
            colors={{ scheme: 'accent' }}
            legends={[
                {
                    anchor: 'right',
                    direction: 'column',
                    translateX: 50,
                    itemWidth: 0,
                    itemHeight: 50,
                    itemTextColor: 'black',
                    symbolSize: 24,
                    symbolShape: 'circle',
                }
            ]}
        />;

        const originCountryStatsPie = <ResponsiveChoropleth
            data={this.state.originCountryStats}
            features={features.features}
            margin={{ top: 40, right: 250, bottom: 40, left: 20 }}
            colors="greens"
            domain={[ 0, this.state.planes.length ]}
            unknownColor="#666666"
            label="properties.name"
            valueFormat=".4s"
            projectionRotation={[ 0, 0, 0 ]}
            enableGraticule={true}
            graticuleLineColor="#dddddd"
            borderWidth={0.5}
            borderColor="#152538"
            legends={[
                {
                    anchor: 'right',
                    direction: 'column',
                    translateX: -200,
                    itemWidth: 0,
                    itemHeight: 18,
                    itemTextColor: '#444444',
                    symbolSize: 24
                }
            ]}
        />;

        const velocityStatsPie = <ResponsiveBar
            data={this.state.velocityStats}
            keys={[ 'count' ]}
            indexBy="velocity"
            margin={{ top: 50, right: 130, bottom: 50, left: 60 }}
            padding={0.3}
            colors={{ scheme: 'accent' }}
            axisTop={null}
            axisRight={null}
            axisBottom={{
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
                legend: 'velocity',
                legendPosition: 'middle',
                legendOffset: 32
            }}
            axisLeft={{
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
                legend: 'planes',
                legendPosition: 'middle',
                legendOffset: -40
            }}
            labelSkipWidth={12}
            labelSkipHeight={12}
            legends={[
                {
                    dataFrom: 'keys',
                    anchor: 'bottom-right',
                    direction: 'column',
                    justify: false,
                    translateX: 120,
                    translateY: 0,
                    itemsSpacing: 2,
                    itemWidth: 100,
                    itemHeight: 20,
                    itemDirection: 'left-to-right',
                    itemOpacity: 0.85,
                    symbolSize: 20
                }
            ]}
            animate={true}
            motionStiffness={90}
            motionDamping={15}
        />;

        let commonRoutes;
        let display;

        if(this.state.commonRoutes.length !== 0) {
            commonRoutes = this.state.commonRoutes.map((info, index) => (
                <li key={index} className="rankingElement"><b>{info.count}</b> from {info.departureAirportName} ({info.departureAirport}) to {info.arrivalAirportName} ({info.arrivalAirport})</li>
            ));
            display = <ul className="rankingList">{commonRoutes}</ul>;
        } else {
            display = <div className="loadingCommonRoutes"><Loading width="20px" height="50px" /><h3>Fetching common routes data . . .</h3></div>;
        }


        return (
            <div className="container">
                <div className="filter">
                    <Filter handleSubmit={this.handleFilter}/>
                </div>
                <div className="generic groundStats">
                    <h3>Planes State</h3>
                    {onGroundStatsPie}
                </div>

                <div className="generic originCountryStats">
                    <h3>Planes Distribution by Origin Country</h3>
                    {originCountryStatsPie}
                </div>
                <div className="generic velocityStats">
                    <h3>Planes Velocity</h3>
                    {velocityStatsPie}

                </div>
                <div className="generic routesRankingStats">
                    <h3>Most Common Routes</h3>
                    {
                        display
                    }
                </div>
            </div>
        );
    }
}

export default Statistics;